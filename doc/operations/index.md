---
stage: Monitor
group: Observability
description: Track errors, application performance issues, and manage incident response.
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Monitor your application

Visualize and analyze errors, traces, metrics and logs collected from your application and its infrastructure. Monitor, identify and resolve performance issues and incidents collaboratively.

|--|--|--|
| [**Getting started**](../user/get_started/get_started_monitoring.md) **{chevron-right}**<br><br>Overview of how features fit together. | [**Error tracking**](error_tracking.md) **{chevron-right}**<br><br>Error tracking, logging, debugging, data retention. | [**Distributed tracing**](tracing.md) **{chevron-right}**<br><br>Monitoring, troubleshooting, performance analysis, request tracking. |
| [**Metrics**](metrics.md) **{chevron-right}**<br><br>Monitoring, visualization, aggregation, analysis, retention. | [**Logs**](logs.md) **{chevron-right}**<br><br>Centralized logging, analysis, configuration, viewing, filtering. | [**Incident management**](incident_management/index.md) **{chevron-right}**<br><br>Alert handling, response coordination, escalation procedures. |
