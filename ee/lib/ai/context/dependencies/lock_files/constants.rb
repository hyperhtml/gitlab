# frozen_string_literal: true

module Ai
  module Context
    module Dependencies
      module LockFiles
        module Constants
          LOCK_FILE_CLASSES = [
            LockFiles::RubyGems
          ].freeze
        end
      end
    end
  end
end
