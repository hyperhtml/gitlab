export const mockData = {
  data: {
    project: {
      id: 'foo/bar',
      issues: {
        nodes: [
          {
            id: 'gid://gitlab/Issue/645',
            title: 'Issue created from app.ads.ad_requests',
            webPath: '/flightjs/Flight/-/issues/38',
          },
          {
            id: 'gid://gitlab/Issue/499',
            title: 'Dismiss Cipher with no integrity',
            webPath: '/flightjs/Flight/-/issues/37',
          },
        ],
      },
    },
  },
};
