---
stage: Deploy
group: Environments
description: Terraform and Kubernetes deployments.
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Manage your infrastructure

Use GitLab to speed up and simplify your infrastructure management practices.

| | | |
|--|--|--|
| [**Getting started**](../get_started/get_started_managing_infrastructure.md) **{chevron-right}**<br><br>Overview of how features fit together. | [**Infrastructure as Code**](iac/index.md) **{chevron-right}**<br><br>Infrastructure management, versioning, automation, state storage, modules. | [**Create Kubernetes clusters**](../clusters/create/index.md) **{chevron-right}**<br><br>Amazon EKS, Azure AKS, Google GKE, Civo. |
| [**Connect Kubernetes clusters**](../clusters/agent/index.md) **{chevron-right}**<br><br>Kubernetes integration, GitOps, CI/CD, agent deployment, cluster management. | [**Runbooks**](../project/clusters/runbooks/index.md) **{chevron-right}**<br><br>Executable runbooks, automation, troubleshooting, operations. | |
