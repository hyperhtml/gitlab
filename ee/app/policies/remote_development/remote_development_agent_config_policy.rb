# frozen_string_literal: true

module RemoteDevelopment
  class RemoteDevelopmentAgentConfigPolicy < BasePolicy
    condition(:can_read_cluster_agent) { can?(:read_cluster_agent, agent) }

    rule { can_read_cluster_agent }.enable :read_remote_development_agent_config

    private

    def agent
      @subject.agent
    end
  end
end
